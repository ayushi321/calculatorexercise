package CalculatorExercise;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class Serializer {
    private String filepath = "/home/ayushi/file.ser";
    private Calculator calculator = new Calculator(12,10);

    public void serialize(){
        try {
            FileOutputStream file = new FileOutputStream(filepath);
            ObjectOutputStream out = new ObjectOutputStream(file);

            out.writeObject(calculator);
            out.close();
            file.close();

            System.out.println("Object has been serialized");
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }
}

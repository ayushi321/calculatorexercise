package CalculatorExercise;

import java.io.Serializable;

public class Calculator implements Serializable {

     private double num1;
     private double num2 ;
     private double result;

    public Calculator(double num1, double num2) {
        this.num1 = num1;
        this.num2 = num2;
    }

    // Methods
    public void Add() {
        result = num1 + num2;
        System.out.println("Addition of two numbers is :" +result);
    }

    public void Subtract() {
        result = num1 - num2;
        System.out.println("Subtraction of two numbers is :" +result);
    }

    public void Multiplication() {
        result = num1 * num2;
        System.out.println("Multiplication of two numbers is :" +result);
    }

    public strictfp void Divide() {
        result = num1 / num2;
        System.out.println("Division of two numbers is :" +result);
    }

// Getter and Setter
    public double getNum1() {
        return num1;
    }

    public double getNum2() {
        return num2;
    }

    public double getResult() {
        return result;
    }

    public void setResult(double result) {
        this.result = result;
    }
}

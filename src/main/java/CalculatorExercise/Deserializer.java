package CalculatorExercise;


import java.io.FileInputStream;
import java.io.ObjectInputStream;


public class Deserializer {
    private String filepath = "/home/ayushi/file.ser";
    private Calculator calcobject = null;

    public void deserializer(){

        try {
            FileInputStream file = new FileInputStream(filepath);
            ObjectInputStream in = new ObjectInputStream(file);
            calcobject = (Calculator)in.readObject();

            in.close();
            file.close();

            calcobject.Add();
            calcobject.Subtract();
            calcobject.Multiplication();
            calcobject.Divide();

            System.out.println("Object has been Deserialized");

        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
